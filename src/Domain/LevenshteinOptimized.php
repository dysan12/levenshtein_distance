<?php
declare(strict_types=1);

/**
 * User: Michal Gaj
 * Date: 25.07.18
 */

namespace Levenshtein\Domain;

use Levenshtein\Domain\VO\Distance;

class LevenshteinOptimized
{
    /**
     * @var string
     */
    private $mainSequence;

    public function __construct(string $mainSequence)
    {
        $this->mainSequence = $mainSequence;
    }

    /**
     * @param array|string[] $dataSet
     * @param int|null $maxDistance
     * @return Distance\Collection|Distance\LevenshteinDistance[]
     */
    public function getDistances(array $dataSet, ?int $maxDistance = null): Distance\Collection
    {
        $distances = new Distance\Collection();
        foreach ($dataSet as $item) {
            try {
                $distance = $this->calculateValuesDistance((string)$item, $maxDistance);
                $distances->add(new Distance\LevenshteinDistance($this->mainSequence, (string)$item, $distance));

            } catch (\RuntimeException $exception) {
                // comp sequence is too different
            }
        }

        return $distances;
    }

    public function getDistance(string $compareSequence): Distance\LevenshteinDistance
    {
        $levArray = [];

        $mainSeqLen = strlen($this->mainSequence);
        $compareSeqLen = strlen($compareSequence);

        for ($i = 0; $i <= $mainSeqLen; $i++) {
            $levArray[$i][0] = $i;
        }

        for ($i = 0; $i <= $compareSeqLen; $i++)
        {
            $levArray[0][$i] = $i;
        }

        for ($i = 1; $i <= $mainSeqLen; $i++) {
            for ($j = 1; $j <= $compareSeqLen; $j++) {
                $cost = 1;
                if ($this->mainSequence[$i - 1] === $compareSequence[$j - 1]) {
                    $cost = 0;
                }

                $levArray[$i][$j] = min(
                    $levArray[$i - 1][$j] + 1,
                    $levArray[$i][$j - 1] + 1,
                    $levArray[$i - 1][$j - 1] + $cost
                );
            }
        }

        return new Distance\LevenshteinDistance($this->mainSequence, $compareSequence, $levArray[$mainSeqLen][$compareSeqLen]);
    }

    private function calculateValuesDistance(string $compareSequence, ?int $maxDistance = null): int
    {
        $levArray = [];

        $mainSeqLen = strlen($this->mainSequence);
        $compareSeqLen = strlen($compareSequence);

        for ($i = 0; $i <= $mainSeqLen; $i++) {
            $levArray[$i][0] = $i;
        }

        for ($i = 0; $i <= $compareSeqLen; $i++)
        {
            $levArray[0][$i] = $i;
        }

        $hasTheSameLength = $mainSeqLen === $compareSeqLen;
        for ($i = 1; $i <= $mainSeqLen; $i++) {
            for ($j = 1; $j <= $compareSeqLen; $j++) {
                $cost = 1;
                if ($this->mainSequence[$i - 1] === $compareSequence[$j - 1]) {
                    $cost = 0;
                }

                $levArray[$i][$j] = min(
                    $levArray[$i - 1][$j] + 1,
                    $levArray[$i][$j - 1] + 1,
                    $levArray[$i - 1][$j - 1] + $cost
                );
            }

            $isDistanceGreater = false;
            if ($hasTheSameLength) {
                $isDistanceGreater = $levArray[$i][$i] > $maxDistance;
            } elseif ($mainSeqLen < $compareSeqLen) {
                $isDistanceGreater = $levArray[$i][$i + ($compareSeqLen - $mainSeqLen)] > $maxDistance;
            } else {
                if ($i > ($mainSeqLen - $compareSeqLen)) {
                    $isDistanceGreater = $levArray[$i][$i - ($mainSeqLen - $compareSeqLen)] > $maxDistance;
                }
            }

            if ($isDistanceGreater) {
                throw new \RuntimeException('The compare sequence is too different');
            }
        }

        return $levArray[$mainSeqLen][$compareSeqLen];
    }
}