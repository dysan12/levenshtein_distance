<?php
declare(strict_types=1);

/**
 * User: Michal Gaj
 * Date: 25.07.18
 */

namespace Levenshtein\Domain\VO\Distance;

final class LevenshteinDistance
{
    /**
     * @var int
     */
    private $distance;
    /**
     * @var string
     */
    private $mainSequence;
    /**
     * @var string
     */
    private $compareSequence;

    public function __construct(string $mainSequence, string $compareSequence, int $distance)
    {
        $this->distance = $distance;
        $this->mainSequence = $mainSequence;
        $this->compareSequence = $compareSequence;
    }

    /**
     * @return int
     */
    public function getDistance(): int
    {
        return $this->distance;
    }

    /**
     * @return string
     */
    public function getMainSequence(): string
    {
        return $this->mainSequence;
    }

    /**
     * @return string
     */
    public function getCompareSequence(): string
    {
        return $this->compareSequence;
    }

    public function isEqual(LevenshteinDistance $levenshteinDistance)
    {
        return (
            $this->getDistance() === $levenshteinDistance->getDistance() &&
            $this->mainSequence === $levenshteinDistance->getMainSequence() &&
            $this->compareSequence === $levenshteinDistance->getCompareSequence()
        );
    }
}