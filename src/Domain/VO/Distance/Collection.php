<?php
declare(strict_types=1);

/**
 * User: Michal Gaj
 * Date: 25.07.18
 */

namespace Levenshtein\Domain\VO\Distance;

use Iterator;

final class Collection implements Iterator, \Countable, \ArrayAccess
{
    private $data = [];

    public function current()
    {
        return current($this->data);
    }

    public function next(): void
    {
        next($this->data);
    }

    public function key()
    {
        return key($this->data);
    }

    public function valid()
    {
        return key($this->data) !== null;
    }

    public function rewind(): void
    {
        reset($this->data);
    }

    public function add(LevenshteinDistance $levenshteinDistance)
    {
        $this->data[] = $levenshteinDistance;
    }

    public function count()
    {
        return count($this->data);
    }

    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->data[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if ($offset) {
            $this->data[$offset] = $value;
        } else {
            $this->data[] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }
}