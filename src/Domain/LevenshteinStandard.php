<?php
declare(strict_types=1);

/**
 * User: Michal Gaj
 * Date: 25.07.18
 */

namespace Levenshtein\Domain;

use Levenshtein\Domain\VO\Distance;

class LevenshteinStandard
{
    /**
     * @var string
     */
    private $mainSequence;

    public function __construct(string $mainSequence)
    {
        $this->mainSequence = $mainSequence;
    }

    /**
     * @param array|string[] $dataSet
     * @param int|null $maxDistance
     * @return Distance\Collection|Distance\LevenshteinDistance[]
     */
    public function getDistances(array $dataSet, ?int $maxDistance = null): Distance\Collection
    {
        $distances = new Distance\Collection();
        foreach ($dataSet as $item) {
            $distance = $this->calculateValuesDistance((string)$item);
            if ($distance <= $maxDistance) {
                $distances->add(new Distance\LevenshteinDistance($this->mainSequence, (string)$item, $distance));
            }
        }

        return $distances;
    }

    public function getDistance(string $compareSequence): Distance\LevenshteinDistance
    {
        $lev = levenshtein($this->mainSequence, $compareSequence);

        return new Distance\LevenshteinDistance($this->mainSequence, $compareSequence, $lev);
    }

    private function calculateValuesDistance(string $compareSequence): int
    {
        return levenshtein($this->mainSequence, $compareSequence);
    }
}