<?php
declare(strict_types=1);
/**
 * User: Michal Gaj
 * Date: 30.07.18
 */

namespace Test;

use Levenshtein\Domain\VO\Distance\LevenshteinDistance;
use PHPUnit\Framework\TestCase;

class LevenshteinDistanceTest extends TestCase
{
    public function testIsEqual_CompareObjectWithTheSameValues_ReturnsTrue()
    {
        $elem1 = new LevenshteinDistance('qwe', 'asd', 5);
        $elem2 = new LevenshteinDistance('qwe', 'asd', 5);

        $this->assertEquals(true, $elem1->isEqual($elem2));
    }

    public function testIsEqual_CompareObjectWithDifferentMainSeq_ReturnsFalse()
    {
        $elem1 = new LevenshteinDistance('qwe', 'asd', 5);
        $elem2 = new LevenshteinDistance('qwewee', 'asd', 5);

        $this->assertEquals(false, $elem1->isEqual($elem2));
    }

    public function testIsEqual_CompareObjectWithDifferentCompareSeq_ReturnsFalse()
    {
        $elem1 = new LevenshteinDistance('qwe', 'asd', 5);
        $elem2 = new LevenshteinDistance('qwe', 'astrtrtd', 5);

        $this->assertEquals(false, $elem1->isEqual($elem2));
    }

    public function testIsEqual_CompareObjectWithDifferentDistance_ReturnsFalse()
    {
        $elem1 = new LevenshteinDistance('qwe', 'asd', 5);
        $elem2 = new LevenshteinDistance('qwe', 'asd', 9);

        $this->assertEquals(false, $elem1->isEqual($elem2));
    }
}
