<?php
declare(strict_types=1);
/**
 * User: Michal Gaj
 * Date: 30.07.18
 */

namespace Test;

use Levenshtein\Domain\VO\Distance\Collection;
use Levenshtein\Domain\VO\Distance\LevenshteinDistance;
use PHPUnit\Framework\TestCase;

class DistanceCollectionTest extends TestCase
{
    public function testCollection_WithElements_Iterates()
    {
        /** @var Collection|LevenshteinDistance[] $collection */
        $collection = new Collection();

        $elements = [
            new LevenshteinDistance('qwe', 'asd', 3),
            new LevenshteinDistance('qwe', 'asd', 4),
            new LevenshteinDistance('qwe', 'asd', 5),
        ];

        $collection->add($elements[0]);
        $collection->add($elements[1]);
        $collection->add($elements[2]);

        foreach ($collection as $key => $item) {
            $this->assertEquals(true, $item->isEqual($elements[$key]));
        }
    }

    public function testCollection_WithElements_CountProperly()
    {
        $collection = new Collection();

        $collection->add(new LevenshteinDistance('qwe', 'asd', 4));
        $collection->add(new LevenshteinDistance('qwe', 'asd', 4));

        $this->assertCount(2, $collection);
    }
}
